[HTML5 Boilerplate homepage](https://html5boilerplate.com) | [Documentation
table of contents](TOC.md)

# Extend and customise HTML5 Boilerplate

Here is some useful advice for how you can make your project with HTML5
Boilerplate even better. We don't want to include it all by default, as
not everything fits with everyone's needs.


* [DNS prefetching](#dns-prefetching)
* [Google Universal Analytics](#google-universal-analytics)
* [Miscellaneous](#miscellaneous)
* [URLs](#urls)


## DNS prefetching

In short, DNS Prefetching is a method of informing the browser of domain names
referenced on a site so that the client can resolve the DNS for those hosts,
cache them, and when it comes time to use them, have a faster turn around on
the request.

### Implicit prefetches

There is a lot of prefetching done for you automatically by the browser. When
the browser encounters an anchor in your html that does not share the same
domain name as the current location the browser requests, from the client OS,
the IP address for this new domain. The client first checks its cache and
then, lacking a cached copy, makes a request from a DNS server. These requests
happen in the background and are not meant to block the rendering of the
page.

The goal of this is that when the foreign IP address is finally needed it will
already be in the client cache and will not block the loading of the foreign
content. Fewer requests result in faster page load times. The perception of this
is increased on a mobile platform where DNS latency can be greater.

#### Disable implicit prefetching

```html
<meta http-equiv="x-dns-prefetch-control" content="off">
```

Even with X-DNS-Prefetch-Control meta tag (or http header) browsers will still
prefetch any explicit dns-prefetch links.

**_WARNING:_** THIS MAY MAKE YOUR SITE SLOWER IF YOU RELY ON RESOURCES FROM
FOREIGN DOMAINS.

### Explicit prefetches

Typically the browser only scans the HTML for foreign domains. If you have
resources that are outside of your HTML (a javascript request to a remote
server or a CDN that hosts content that may not be present on every page of
your site, for example) then you can queue up a domain name to be prefetched.

```html
<link rel="dns-prefetch" href="//example.com">
<link rel="dns-prefetch" href="https://ajax.googleapis.com">
```

You can use as many of these as you need, but it's best if they are all
immediately after the [Meta
Charset](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/meta#attr-charset)
element (which should go right at the top of the `head`), so the browser can
act on them ASAP.

#### Common Prefetch Links

Amazon S3:

```html
<link rel="dns-prefetch" href="//s3.amazonaws.com">
```

Google APIs:

```html
<link rel="dns-prefetch" href="https://ajax.googleapis.com">
```

Microsoft Ajax Content Delivery Network:

```html
<link rel="dns-prefetch" href="//ajax.microsoft.com">
<link rel="dns-prefetch" href="//ajax.aspnetcdn.com">
```

### Further reading about DNS prefetching

* https://developer.mozilla.org/en-US/docs/Controlling_DNS_prefetching
* https://dev.chromium.org/developers/design-documents/dns-prefetching
* https://blogs.msdn.microsoft.com/ie/2011/03/17/internet-explorer-9-network-performance-improvements/
* http://dayofjs.com/videos/22158462/web-browsers_alex-russel


## Google Universal Analytics

### More tracking settings

The [optimized Google Universal Analytics
snippet](https://mathiasbynens.be/notes/async-analytics-snippet#universal-analytics)
included with HTML5 Boilerplate includes something like this:

```js
ga('create', 'UA-XXXXX-X', 'auto'); ga('send', 'pageview');
```

To customize further, see Google's [Advanced
Setup](https://developers.google.com/analytics/devguides/collection/analyticsjs/advanced),
[Pageview](https://developers.google.com/analytics/devguides/collection/analyticsjs/pages),
and [Event](https://developers.google.com/analytics/devguides/collection/analyticsjs/events) Docs.

### Anonymize IP addresses

In some countries, no personal data may be transferred outside jurisdictions
that do not have similarly strict laws (i.e. from Germany to outside the EU).
Thus a webmaster using the Google Universal Analytics may have to ensure that
no personal (trackable) data is transferred to the US. You can do that with
[the `ga('set', 'anonymizeIp', true);`
parameter](https://developers.google.com/analytics/devguides/collection/analyticsjs/advanced#anonymizeip)
before sending any events/pageviews. In use it looks like this:

```js
ga('create', 'UA-XXXXX-X', 'auto');
ga('set', 'anonymizeIp', true);
ga('send', 'pageview');
```

### Track JavaScript errors in Google Analytics

Add this function after `ga` is defined:

```js
(function(window){
    var undefined,
        link = function (href) {
            var a = window.document.createElement('a');
            a.href = href;
            return a;
        };
    window.onerror = function (message, file, line, column) {
        var host = link(file).hostname;
        ga('send', {
          'hitType': 'event',
          'eventCategory': (host == window.location.hostname || host == undefined || host == '' ? '' : 'external ') + 'error',
          'eventAction': message,
          'eventLabel': (file + ' LINE: ' + line + (column ? ' COLUMN: ' + column : '')).trim(),
          'nonInteraction': 1
        });
    };
}(window));
```

## Search

### Direct search spiders to your sitemap

After creating a [sitemap](https://www.sitemaps.org/protocol.html)

Submit it to search engine tool:
* [Google](https://www.google.com/webmasters/tools/sitemap-list)
* [Bing](https://www.bing.com/toolbox/webmaster)
* [Yandex](https://webmaster.yandex.com/)
* [Baidu](https://zhanzhang.baidu.com/)
OR
Insert the following line anywhere in your robots.txt file, specifying the path to your sitemap:
```
Sitemap: https://example.com/sitemap_location.xml
```

## Miscellaneous

* Use [Microformats](http://microformats.org/wiki/Main_Page) (via
  [microdata](http://microformats.org/wiki/microdata)) for optimum search
  results
  [visibility](https://webmasters.googleblog.com/2009/05/introducing-rich-snippets.html).

## URLs

### Official shortlink

Signal to the world "This is the shortened URL to use this page!" Poorly
supported at this time. Learn more by reading the [article about shortlinks on
the Microformats wiki](http://microformats.org/wiki/rel-shortlink).

```html
<link rel="shortlink" href="h5bp.com">
```
