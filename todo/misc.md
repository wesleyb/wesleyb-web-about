[HTML5 Boilerplate homepage](https://html5boilerplate.com/) | [Documentation
table of contents](TOC.md)

# Miscellaneous

* [Server Configuration](#server-configuration)

--

## Server Configuration

H5BP includes a [`.htaccess`](#htaccess) file for the [Apache HTTP
server](https://httpd.apache.org/docs/). If you are not using Apache
as your web server, then you are encouraged to download a
[server configuration](https://github.com/h5bp/server-configs) that
corresponds to your web server and environment.
