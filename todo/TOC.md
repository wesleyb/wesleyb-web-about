[HTML5 Boilerplate homepage](https://html5boilerplate.com/)

## HTML5 Boilerplate core

* [Everything else](misc.md).

## Development

* [Extending and customizing HTML5 Boilerplate](extend.md) — Going further
  with the boilerplate.

## Related projects

The [H5BP organization](https://github.com/h5bp) maintains several projects
that complement HTML5 Boilerplate, projects that can help you improve different
aspects of your website/web app (e.g.: the performance, security, etc.).

* [Server Configs](https://github.com/h5bp/server-configs) — Fast and
  smart configurations for web servers such as Apache and Nginx.
    *  [Apache](https://github.com/h5bp/server-configs-apache)
    *  [Google App Engine (GAE)](https://github.com/h5bp/server-configs-gae)
    *  [Internet Information Services (IIS)](https://github.com/h5bp/server-configs-iis)
    *  [lighttpd](https://github.com/h5bp/server-configs-lighttpd)
    *  [Nginx](https://github.com/h5bp/server-configs-nginx)
    *  [Node.js](https://github.com/h5bp/server-configs-node)
