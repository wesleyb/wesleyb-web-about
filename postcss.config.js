/**
 * WES CSS - PostCSS Configuration
 * @see [PostCSS](http://postcss.org/)
 * @see [PostCSS: Configuration](https://github.com/postcss/postcss-loader#usage)
 * @module config/postCSS
 */
"use strict";

const fs = require('fs');
const path = require('path');
const glob = require('glob');

const cssnanoConfig = require('./cssnano.config.js');

const NODE_ENV = process.env.NODE_ENV;
const CSS_BUNDLER = process.env.CSS_BUNDLER;

/**
 * Empty previously exported settings
 * @param {String[]} filePaths - Paths of files into which to export settings
 */
// RFE: Find a simpler way to do this, without adding dependencies.
//      - An NPM script run via `exec` is the same overhead as direct Node.
//      - If you use invest in `shelljs`, then use it here to simplify this.
function resetExportFiles( filePaths ) {
  filePaths = [].concat( filePaths ); // Force parameter into an array

  filePaths.forEach( filePath => {
    const emptyFileMessage = 'Emptied via `postcss.config.js` (to ensure old styles are not used during build)';

    filePath = path.join(__dirname, filePath);
    if ( fs.existsSync( filePath )) {
      fs.writeFileSync(filePath, `/* ${emptyFileMessage} */`);
    }
  });
}

/**
 * Build list of settings to import
 * @see https://github.com/csstools/postcss-preset-env#importfrom
 * @return {Array}
 */
function buildImportList() {
  // WARNING: Success depends on manner of entry, format, and order…
  // SEE: https://stackoverflow.com/a/57139154/11817077
  // FAQ: Explicitely provide settings, because Parcel import is buggy…
  // SEE: https://github.com/parcel-bundler/parcel/issues/1165"
  const importListData = [
    {
      "customSelectors": {
        // FAQ: PostCSS may inject spaces into complex custom selectors
        // SEE: https://github.com/postcss/postcss-custom-selectors/issues/40
        ":--global-form-ux-element": "input,select,textarea,button",
        ":--global-heading-element": "h1,h2,h3,h4,h5,h6",
        ":--global-list-element": "ul,ol,dl",
        ":--global-title-element--block": "label[title]",
        // No `[title]` for `abbr` to avoid space injection bug of pluign
        ":--global-title-element--inline": "a[title],abbr",
        ":--global-title-element--link": "a[title]",
        // Add `[title]` for `abbr` to raise specificity to match borwser style
        ":--global-title-element--abbr": "abbr[title]",
        ":--global-title-element--any": "[title]"
      },
      "environmentVariables": {
        "--phi": "1.618",

        "--vw--narrow":         "300px",
        "--vw--narrow-medium":  "485px",
        "--vw--medium":         "670px",
        "--vw--medium-wide":    "1085px",
        "--vw--wide":           "1500px",

        "--space--xxx-small":   "0.25rem",
        "--space--xx-small":    "0.4rem",
        "--space--x-small":     "0.6rem",
        "--space--small":       "1.0rem",
        "--space--normal":      "1.8rem",
        "--space--large":       "3.0rem",
        "--space--x-large":     "4.8rem",
        "--space--xx-large":    "8.0rem",
      },
    }
  ];
  const importListPaths = glob.sync(
    './source/styles/_imports/settings/*', {
      ignore: [
        './**/env.*.css',
        './**/select.*.css',
        './**/README.css',
        './**/*disabled*',
        './**/*docs*'
      ]
  });

  return [ ...importListData, ...importListPaths ];
}

const importPath = './source';
const importFrom = buildImportList();
const exportTo = './source/styles/_exports/settings.css';
const mixinsDir = './source/styles/_imports/tools/mixins';

resetExportFiles(exportTo);

/* WARNING: Parcel does not seem to allow `(ctx) => ({…})` */
module.exports = {
  map: (NODE_ENV === 'development'),
  plugins: {
    /* FAQ: "Parcel processes each CSS file independently",
            so imported variables will not be replaced */
    /* SEE: https://github.com/parcel-bundler/parcel/issues/1165 */
    'postcss-import': (CSS_BUNDLER !== 'parcel') ? {
      path: importPath
    } : false,

    // Load only what `.browserslistrc` requires from `normalize.css`
    'postcss-normalize': true,

    // Support mixins
    'postcss-mixins': {
      mixinsDir: mixinsDir
    },

    // Support tomorrow's CSS, today
    'postcss-preset-env': {
      stage: 0,
      preserve: false,
      importFrom: importFrom,
      // ???: Does not seem to be necessary for Parcel nor PostCSS — 2019-07-23
      // exportTo: exportTo,
    },

    // Support `@extend` at-rule
    // NOTE: To extend a selector that is built via nesting, then
    //       extend AFTER nesting polyfill, otherwise extention fails
    'postcss-extend-rule': {
      onRecursiveExtend: 'throw',
      onUnusedExtend: 'warn'
    },

    // Minify CSS
    'cssnano': (NODE_ENV === 'production') ? cssnanoConfig : false,

    // Report PostCSS issues in browser
    'postcss-browser-reporter': true,

    // Report PostCSS issues in CLI
    'postcss-reporter': true
  }
};
