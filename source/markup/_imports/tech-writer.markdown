{{shared/comment.o-app.html}}
<div class="o-app--layout-vert">
<header class="o-app__head c-intro">

# [Wesley B][wes-index-short]

<h2 class="c-intro__major">
    <span>Technical Writer</span>
    <small class="c-intro__extra">Sr. Front-End Engineer</small>
</h2>
<p>
This page explains my extensive work in technical and non-technical documentation for numerous Front-End Engineering projects from five sibling companies. In short, I have created, updated, and maintained documentation that initially came to me inconsistent, incomplete, and independent but has since become a navigable, consistent, reliable explanation of almost all responsibilities of an entire department.
</p>

{{shared/instructions.md}}

{{shared/glossary.md}}

</header>
<section class="o-app__body">
<article class="o-page__article s-ability-list--shallow">

## Front-End Documentation

### Internal Team

For my individual and team projects, I organized, created, updated, maintained (and, later, delegated the aforementioned tasks) these kinds of docs:

- process & instructions
- repository `README.md`'s
- wiki-maintained instructions
- cross-project modules
- standards & best practices
- list and explain internal ones
- organize links to external ones
- reference source material

### External Teams

For projects with consumers from other teams (back-end, DevOps, architects, marketing, HR, etc), I created (and trained others to create) these kinds of docs:

- introductions & instructions
- technical and non-technical

### Project Management

For each project I managed, I maintained these kinds of docs:

- research
- status & estimation
- requirements (high-level)
- requirements (low-level)

### Community of Practice

I created and participated in two [Communities of Practice](https://en.wikipedia.org/wiki/Community_of_practice)^*^, _Front-End Development_ and _Software Engineering_, for which I maintained (or delegated creation of) these kinds of docs:

- lesson material
- session notes
- topics of interest

<small>^*^ I also inspired a few others.</small>

</div>
</article>
</section>
<footer class="o-app__foot">

{{shared/footer.md}}

</footer>
</div>
