
<details class="c-skill" id="framework--aurelia">
    <summary class="c-skill__title">
        <h3 class="js-ignore"
            id="framework--aurelia--title">Aurelia</h3>
    </summary>
    <ul class="c-gauge--layout-stacked has-range--count-2 c-skill__data">
        <li class="c-gauge__range is-high">
            <label id="framework--aurelia--current-skill--label"
                for="framework--aurelia--current-skill"
                class="c-gauge__label"
                title="{{shared/title.current-proficiency.md}}">Current Proficiency</label>
            <progress id="framework--aurelia--current-skill"
                class="c-gauge__figure"
                aria-labelledby="framework--aurelia--title framework--aurelia--current-skill--label"
                value="4" max="5">
                <span class="c-gauge__desc">4 out of 5, relative to other disciplines</span>
            </progress>
        </li>
        <li class="c-gauge__range is-high">
            <label id="framework--aurelia--future-skill--label"
                for="framework--aurelia--future-skill"
                class="c-gauge__label"
                title="{{shared/title.future-proficiency.md}}">Projected Proficiency</label>
            <progress id="framework--aurelia--future-skill"
                class="c-gauge__figure"
                aria-labelledby="framework--aurelia--title framework--aurelia--future-skill--label"
                value="5" max="5">
                <span class="c-gauge__desc">5 out of 5, relative to other disciplines</span>
            </progress>
        </li>
    </ul>
    <p class="c-skill__notes">
{{js-frameworks/notes/aurelia.md}}
    </p>
</details>

<details class="c-skill" id="framework--react">
    <summary class="c-skill__title">
        <h3 class="js-ignore"
            id="framework--react--title">React</h3>
    </summary>
    <ul class="c-gauge--layout-stacked has-range--count-2 c-skill__data">
        <li class="c-gauge__range is-low">
            <label id="framework--react--current-skill--label"
                for="framework--react--current-skill"
                class="c-gauge__label"
                title="{{shared/title.current-proficiency.md}}">Current Proficiency</label>
            <progress id="framework--react--current-skill"
                class="c-gauge__figure"
                aria-labelledby="framework--react--title framework--react--current-skill--label"
                value="1" max="5">
                <span class="c-gauge__desc">1 out of 5, relative to other disciplines</span>
            </progress>
        </li>
        <li class="c-gauge__range is-max">
            <label id="framework--react--future-skill--label"
                for="framework--react--future-skill"
                class="c-gauge__label"
                title="{{shared/title.future-proficiency.md}}">Projected Proficiency</label>
            <progress id="framework--react--future-skill"
                class="c-gauge__figure"
                aria-labelledby="framework--react--title framework--react--future-skill--label"
                value="5" max="5">
                <span class="c-gauge__desc">5 out of 5, relative to other disciplines</span>
            </progress>
        </li>
    </ul>
    <p class="c-skill__notes">
{{js-frameworks/notes/react.md}}
    </p>
</details>

<details class="c-skill" id="framework--polymer">
    <summary class="c-skill__title">
        <h3 class="js-ignore"
            id="framework--polymer--title">Polymer</h3>
    </summary>
    <ul class="c-gauge--layout-stacked has-range--count-2 c-skill__data">
        <li class="c-gauge__range is-low">
            <label id="framework--polymer--current-skill--label"
                for="framework--polymer--current-skill"
                class="c-gauge__label"
                title="{{shared/title.current-proficiency.md}}">Current Proficiency</label>
            <progress id="framework--polymer--current-skill"
                class="c-gauge__figure"
                aria-labelledby="framework--polymer--title framework--polymer--current-skill--label"
                value="1" max="5">
                <span class="c-gauge__desc">1 out of 5, relative to other disciplines</span>
            </progress>
        </li>
        <li class="c-gauge__range is-high">
            <label id="framework--polymer--future-skill--label"
                for="framework--polymer--future-skill"
                class="c-gauge__label"
                title="{{shared/title.future-proficiency.md}}">Projected Proficiency</label>
            <progress id="framework--polymer--future-skill"
                class="c-gauge__figure"
                aria-labelledby="framework--polymer--title framework--polymer--future-skill--label"
                value="4" max="5">
                <span class="c-gauge__desc">4 out of 5, relative to other disciplines</span>
            </progress>
        </li>
    </ul>
    <p class="c-skill__notes">
{{js-frameworks/notes/polymer.md}}
    </p>
</details>

<details class="c-skill" id="framework--ember">
    <summary class="c-skill__title">
        <h3 class="js-ignore"
            id="framework--ember--title">Ember</h3>
    </summary>
    <ul class="c-gauge--layout-stacked has-range--count-2 c-skill__data">
        <li class="c-gauge__range is-low">
            <label id="framework--ember--current-skill--label"
                for="framework--ember--current-skill"
                class="c-gauge__label"
                title="{{shared/title.current-proficiency.md}}">Current Proficiency</label>
            <progress id="framework--ember--current-skill"
                class="c-gauge__figure"
                aria-labelledby="framework--ember--title framework--ember--current-skill--label"
                value="2" max="5">
                <span class="c-gauge__desc">2 out of 5, relative to other disciplines</span>
            </progress>
        </li>
        <li class="c-gauge__range">
            <label id="framework--ember--future-skill--label"
                for="framework--ember--future-skill"
                class="c-gauge__label"
                title="{{shared/title.future-proficiency.md}}">Projected Proficiency</label>
            <progress id="framework--ember--future-skill"
                class="c-gauge__figure"
                aria-labelledby="framework--ember--title framework--ember--future-skill--label"
                value="3" max="5">
                <span class="c-gauge__desc">3 out of 5, relative to other disciplines</span>
            </progress>
        </li>
    </ul>
    <p class="c-skill__notes">
{{js-frameworks/notes/ember.md}}
    </p>
</details>

<details class="c-skill" id="framework--other">
    <summary class="c-skill__title">
        <h3 class="js-ignore"
            id="framework--other--title">others</h3>
    </summary>
    <aside class="c-skill-notes"><small>The frameworks that I have only evaluated, or not used in a long time, are Angular, Vue, Backbone, JavaScriptMVC</small></aside>
    <ul class="c-gauge--layout-stacked has-range--count-2 c-skill__data">
        <li class="c-gauge__range is-low">
            <label id="framework--other--current-skill--label"
                for="framework--other--current-skill"
                class="c-gauge__label"
                title="{{shared/title.current-proficiency.md}}">Current Proficiency</label>
            <progress id="framework--other--current-skill"
                class="c-gauge__figure"
                aria-labelledby="framework--other--title framework--other--current-skill--label"
                value="1" max="5">
                <span class="c-gauge__desc">1 out of 5, relative to other disciplines</span>
            </progress>
        </li>
        <li class="c-gauge__range is-low">
            <label id="framework--other--future-skill--label"
                for="framework--other--future-skill"
                class="c-gauge__label"
                title="{{shared/title.future-proficiency.md}}">Projected Proficiency</label>
            <progress id="framework--other--future-skill"
                class="c-gauge__figure"
                aria-labelledby="framework--other--title framework--other--future-skill--label"
                value="2" max="5">
                <span class="c-gauge__desc">2 out of 5, relative to other disciplines</span>
            </progress>
        </li>
    </ul>
    <p class="c-skill__notes">
{{js-frameworks/notes/other.md}}
    </p>
</details>
