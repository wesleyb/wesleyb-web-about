
- I evaluated these frameworks on the basis of:
    - template syntax
    - learning curve
    - documentation quality
    - separation of technologies
    - separation of concerns
    - effort towards standards-compliance

- I declined them, because of _any_ of these reasons:
    - outdated
    - bloated
    - project becomes tightly coupled to framework
    - compared poorly against other choices
