
- I initially adopted Aurelia because it:
    - provides a valid-HTML template syntax
    - by default, requires separation of technologies
    - initially, created standards-compliant code
    - supports separation of concerns
    - supports composition in JS via decorators

- I have since retired Aurelia because it:
    - is difficult to update
    - has awful documentation
    - later, deviated from standards-compliant code
