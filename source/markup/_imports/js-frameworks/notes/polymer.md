
- I have been excited to use Polymer, because it:
    - is next-gen valid-HTML syntax
    - strives for standards-compliant code
    - supports separation of concerns
    - supports separation of technologies
    - polyfills web components

- I have been delaying use of Polymer, because it:
    - is unstable
    - is inconsistently documented
