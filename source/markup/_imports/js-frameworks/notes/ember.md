
- I used this in a web application because
    - The application needed the power of Ember.

- I warn against Ember, because:
    - The learning curve is very high.
    - The framework is not forgiving.
    - Unless it meets _almost all_ technical requirements of a project, it may be more investment than its worth.
