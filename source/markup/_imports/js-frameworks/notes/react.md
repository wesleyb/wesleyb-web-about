
- I initially ignored React because it:
    - requires a non-HTML template syntax
    - by default, rejects separation of technologies
    - does not create standards-compliant code

- I have since adopted React because it:
    - the non-HTML template syntax is JS string interpolation
    - supports separation of technologies
    - forces separation of concerns
    - has excellent documentation
    - now, is nigh inescapable in its industry
