{{shared/comment.o-app.html}}
<div class="o-app--layout-vert">
<header class="o-app__head c-intro">

# [Wesley B][wes-index-short]

<h2 class="c-intro__major">
    <span>Sr. Front-End Engineer</span>
    <small class="c-intro__extra">and other roles</small>
</h2>
<p>
I write, architect, and promote maintainable code; plan large projects; research and promote best practices; and provide and foster mentorship in software engineering and all of these skills.
</p>

{{shared/instructions.md}}

{{shared/glossary.md}}

</header>
<section class="o-app__body o-columns">
<article class="o-page__article o-columns__element s-ability-list c-intro">

## Disciplines

<small>Ordered by professional interest.</small>

{{index/disciplines.md}}

</article>
<article class="o-page__article o-columns__element s-ability-list--deep">

## Technical Skills

<small>Ordered by level of expertise.</small>

{{index/skills.md}}

</article>
<article class="o-page__article o-columns__element s-ability-list--shallow">

## Personal Attributes

<small>Ordered by which make me look best.</small>

{{index/attributes.md}}

</article>
<article class="o-page__article o-columns__element s-ability-list--shallow">

## Roles & Responsibilities

<small>Ordered by most recent to least recent.</small>

{{index/roles.md}}

</article>
</section>
<footer class="o-app__foot">

{{shared/footer.md}}

</footer>
</div>
