NOTICE: This file must be included before other files

<!-- RFE: Show glossary when it is rendered usefully and is accessible. -->
<!-- FAQ: MultiMarkdown will "over-render" the first instance of an `<abbr>`,
          so I store that excess information in a proper glossary. -->
<aside id="s-md2html--auto-gen-abbr-glossary" hidden>

- [>projected]
- [>current]
- apps
- epics
- [>"features"]
- [>hybrid mobile app frameworks]
- [>accessibility]
- [>progressive enhancement]
- [>prior JS tooling]
- [>prior CSS organization]
- [>prior CSS processing]
- [>prior CSS documentation]
- [>newer JS scripting]
- [>DOM manipulation]
- [>in-house]
- [>styleguide.js]
- [>earlier CMS experience]
- [>prior CMS experience]
- Chinese
- SASS
- LESS
- OOCSS
- SMACSS
- UML
- CI
- CD
- PHP
- [>sinc.]

</aside>

[>projected]: {{shared/title.future-proficiency.md}}
[>current]: {{shared/title.current-proficiency.md}}
[>apps]: applications
[>epics]: sets of related user stories
[>"features"]: sets of related epics
[>hybrid mobile app frameworks]: Electron, React Native, Ionic, Cordova, AngularJS, NativeScript
[>progressive enhancement]: Preferable to graceful degredation (but not feasible for every project)
[>accessibility]: Often available merely via semantic HTML5 (ARIA roles may be required)
[>prior JS tooling]: Webpack, Bower, Gulp, Grunt, RequireJS
[>prior CSS organization]: OOCSS, SMACSS, in-house
[>prior CSS processing]: SASS, LESS
[>prior CSS documentation]: styleguide.js, in-house
[>newer JS scripting]: Node
[>DOM manipulation]: Preference for raw JavaScript, not jQuery
[>in-house]: I devised a methodology that I do not use, now
[>styleguide.js]: Abandoned project I do not use, now
[>earlier CMS experience]: Shopify, WordPress
[>prior CMS experience]: Joomla, PrestaShop, Magento, Typolight, and more
[>Chinese]: Mandarin
[>SASS]: Syntastically Awesome Style Sheets
[>LESS]: Leaner Style Sheets
[>OOCSS]: Object-Oriented CSS
[>SMACSS]: Scalable & Modular Architecture CSS
[>UML]: Unified Modeling Language
[>CI]: Continuous Integration
[>CD]: Continuous Development
[>PHP]: Don't judge; we all start somewhere; I crawled my way up from the sewer
[>sinc.]: a defunct in-house product I worked on