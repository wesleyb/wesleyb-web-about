<nav class="s-nav">

<!-- RFE: A templating language would allow class on active page -->
- [Home][wes-index]
- [LinkedIn](https://www.linkedin.com/in/wesleybomar "LinkedIn Profile - Wesley B")
- [GitHub](https://github.com/wesleyboar "GitHub Profile - Wesleyboar")
- <small>[JavaScript Frameworks][wes-js-fw]</small>
- <small>[Technical Writer][wes-tech-w]</small>
- <small>[Résumé][wes-fe-resume]</small>

<nav>

[wes-tech-w]: /tech-writer.html "Wesley B - Front-End Documentation"
[wes-js-fw]: /js-frameworks.html "Wesley B - JS Framework Proficiency"
[wes-index]: /index.html "Wesley B - Professional Overview"
[wes-index-short]: /index.html "Professional Overview"
[wes-fe-resume]: /docs/wesleyb-fe-resume-2019.rtf "Wesley B - Front-End Résumé"
[wes-des-resume]: http://yi3artist.com/docs/%57%65%73%6C%65%79%42%6F%6D%61%72%32%30%31%32%2E%70%64%66 "Wesley B - 2012 Résumé"
[wes-des-website]: http://yi3artist.com "Wesley B - 2011 Website"
[wes-des-portfolio]: https://www.deviantart.com/wesleyallen/gallery "Wesley B - 2010 Portfolio"
