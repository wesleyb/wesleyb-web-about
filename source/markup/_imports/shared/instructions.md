<blockquote class="c-intro__minor s-md2html-example">

- Headings with a disclosure triangle (▸), when clicked/tapped/activated, will reveal more information.
    - Some lists can be nested many levels deep. Explore as far as you care.</span>
- <p>Such headings, <mark>if marked (like this)</mark>, have content that I specifically have choosen to highlight.</p>
    - Only the headings are marked. No need to hunt for marked content.

<footer>
    Wesley B, <cite>notes from candidate</cite>
</footer>

</blockquote>