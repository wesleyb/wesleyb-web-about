{{shared/comment.o-app.html}}
<div class="o-app--layout-vert">
<header class="o-app__head c-intro">

# [Wesley B][wes-index-short]

<h2 class="c-intro__major">
    <span>Sr. JavaScript Engineer</span>
    <small class="c-intro__extra">Sr. Front-End Engineer</small>
</h2>
<p>
This page explains my proficiency in JavaScript frameworks for client web applications. It does <strong>not</strong> include libraries, such as jQuery (nor Zepto), Lodash (nor Underscore), various widgets, etc.
</p>

{{shared/instructions.md}}

{{shared/glossary.md}}

</header>
<section class="o-app__body">
<article class="o-page__article s-ability-list">

## JavaScript Frameworks

{{js-frameworks/frameworks.md}}

</article>
</section>
<footer class="o-app__foot">

{{shared/footer.md}}

</footer>
</div>
