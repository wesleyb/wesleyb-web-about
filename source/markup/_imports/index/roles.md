### Jr. Project Manager

- new responsibilities
    - requirements gathering
    - requirements documentation
    - team management
    - project planning
- serving mutliple roles
    - technical product manager
    - software architect
    - jr. project manager

Transitioning to project management, I learned about the balance between business, function, and quality; but, also, the conflicts, constrasts, and time constraints of serving multiple roles.

### <mark>Front-End Architect</mark>

1. shared code across projects
    - focus on CSS components
    - integrate into legacy projects
    - mentor team
1. <p>web apps & marketing websites</p>
    - mentor others in architecture
    - senior engineer
    - cross-team
1. cross-team documentation
    - information architecture
    - content & maintenance
    - mentor others in documentation
1. multiple new large projects
    - multi-project pattern library development
    - split giant legacy website into two websites
    - <p>architect powerful [Craft CMS][craft] implementation</p>
        - information architecture
        - cross-team collaboration
        - layers of internal user experience
            - engineer as user of architecture
            - designer as user of pattern builders
            - marketer as user of page builders
1. technical product manager
    - cross-team communication
    - information architecture
    - project research & planning

I led and mentored an in-house team of developers on many projects at once. I taught them all I knew, and how to exceed me. It was the most rewarding and prolific time of my career, but also the most taxing.

### Software Engineer

1. web app UI development
    - scripting, styling, semantics
    - ui/ux & usability
    - cross-team collaboration
1. developer documentation
    - architecture
    - instructions
    - standards & best practices
1. <p>client-side web apps</p>
    1. file storage service UI
    2. secure messaging service UI
    3. VPN router-based service UI
    4. VPN server administration
    5. VPN product SDK example service UI
    6. network optimization service UI
    7. service account control panel
1. shared code across projects
    - JavaScript & CSS components
    - architect & implement solution
    - integrate into new projects
1. API design and development
    - VPN server administration UI
    - independent personal projects
1. front-end architecture
    - research & development
    - information architecture
    - cross-team development

Transitioning to app development let me evaluate and apply new front-end tech and learn from the experience of my coworkers versed in traditional software engineering. My growth was monumental.

### Front-End Engineer

- marketing website UI engineer
    - five brands
    - development and maintenance
    - cross-team communication

My employment in the United States began as a UI/UX engineer for marketing websites. During this time, I began to avid help the application development team with their web app UI challenges.

### Jack of All Trades

1. Full Stack Web Developer
    - front-end developer
    - back-end PHP developer
    - FileMaker database UI developer
2. Designer
    - logos
    - icons
    - banners
3. I.T. Asst. Manager
    - I interpreted for American I.T. manager in a Chinese company and managed, by proxy, the I.T. staff.

My overseas career began in China as a full-stack web developer with a proclivity for design and a passion to help however I could.
