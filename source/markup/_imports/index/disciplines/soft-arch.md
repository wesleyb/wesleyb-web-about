
- <p>__two__ years professional experience</p>
    - three large web projects
    - mentorship & documentation
    - cross-team development
    - research technical solutions
- offered experience in tangential disciplines
    - [Project Management](#discipline--proj-mgmt--title)
    - [Technical Product](#discipline--tech-prod--title)