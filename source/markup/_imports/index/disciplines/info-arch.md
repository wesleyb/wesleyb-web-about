
- sporadic tangential professional experience
    - [organize front-end docs of five companies][wes-tech-w]
    - architect several repositories
    - extrapolate and define design patterns
- <p>_all_ above experience was during tangential discipline</p>
    - [Front-End Engineering](#discipline--fe-dev--title)
- miscellaneous personal experience
    - 2005—2015 off-and-on personal technical project
    - 2019-Q2 personal creative project