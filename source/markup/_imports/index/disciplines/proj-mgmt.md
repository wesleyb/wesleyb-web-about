
- recent tangential professional experience
    - track and report status of involved engineering teams
    - discuss high-level project requirements
    - serve, also, as [Technical Product Manager](#discipline--tech-prod--title) and [Front-End Architect](#markfront-endarchitectmark)
- <p>_all_ above experience was during tangential discipline</p>
    - [Software Architecture](#discipline--soft-arch--title)
- miscellaneous personal experience
    - late 2019 website resume & job hunt