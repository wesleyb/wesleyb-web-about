
- sporadic tangential professional experience
    - create and organize documentation
    - [maintain front-end docs of five companies][wes-tech-w]
    - training and delegation of documentation
- <p>_all_ above experience was during tangential discipline</p>
    - [Front-End Engineering](#discipline--fe-dev--title)