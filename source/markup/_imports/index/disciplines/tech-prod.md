
- <p>_two_ years tangential professional experience</p>
    - acquire/define^*^ requirements for engineers
    - create and organize user stories, epics, and [>"features"]
    - serve, also, as [Jr. Project Manager][] and [Front-End Architect](#markfront-endarchitectmark)
- <p>_all_ above experience was during tangential discipline</p>
    - [Software Architecture](#discipline--soft-arch--title)

^*^ Serving multiple roles blurred the responsibility for requirements.