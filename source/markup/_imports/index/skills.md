TODO: Reduce duplicate content—difficult, given both MMD transclusion behavior and nested lists

### <mark>Professional Expertise</mark>

- front-end engineering
    - HTML
        - HTML5
        - semantics
        - minimal tag count
    - CSS
        - organization & modularity
            - [BEM][]
            - [Bem with Namespaces][BEMNS]
            - [ITCSS][]
            - [BEMIT][] <small>sans "Responsive Suffixes"</small>
            - OOCSS
            - [>prior CSS organization]
        - processing & compatibility
            - [PostCSS][]
            - [>prior CSS processing]
        - documentation & pattern library
            - [KSS][]
            - [>prior CSS documentation]
    - JavaScript
        - ECMAScript 5—6/2015+
            - ECMAScript 5
                <br /><small>The good parts and the bad parts.</small>
            - ECMAScript 2015+
                <br /><small>I know, well, the \~half of it that I have used.</small>
        - tooling
            1. Parcel
            1. Rollup
            1. [>prior JS tooling]
        - scripting
            1. [>DOM manipulation]
            1. NPM
            1. [>newer JS scripting]
        - latest technology
            - `.browserslistrc`
                - For JavaScript via Babel
                - For CSS via PostCSS
            - ES2015+ via Babel
                - I've used its latest featureset for every new project.
                - I've had to integrate it into many tooling setups.
            - [`postcss-preset-env`][postcss-preset-env]
                - This, after having used [cssnext][].
                - I have begun to help the community.
            - Aurelia
                - This is, now, not a recent framework. But, I was an early adopter.
                - I required usage to be explicit, not its default implicit behavior.
        - frameworks
            - See [JavaScript Frameworks][wes-js-fw].
    - usability
        - [>progressive enhancement]
        - [>accessibility]
        - performance
- documentation
    - code
        - standards
            - explain internal standards
            - link to external standards
        - generated API docs
            - [KSS][]
            - [JSDoc][]
            - [phpDocumentor][phpdoc]
        - comments
            - [PEP 350][pep350]
            - [self-documenting code is (mostly) nonsense](https://medium.com/it-dead-inside/self-documenting-code-is-mostly-nonsense-1de5f593810f)
    - pattern library
        - user-facing content
            <br /><small>Great companies will publish quality internal work for the community.</small>
        - developer content
            <br /><small>Developer-specific knowledge can easily have its own auto-generated website.</small>
    - wiki syntax
        - Markdown <small>and its extended family</small>
        - MediaWiki
        - Confluence
    - project management
        - requirements
        - planning
- semantics
    - HTML5
    - documentation
    - pattern library
    - naming in code
    - cross-team vocabulary
- software architecture
    - abstraction
        <br /><small>documentation, consistent vocabulary, no over-engineering</small>
    - reusable code
        <br /><small>Please, ask me about "Shared CSS for Powerhouse Management".</small>
    - mentorship
        <br /><small>communities of practice, socratic method, freedom</small>
- CMS
    - [Craft CMS][craft]
    - [>earlier CMS experience]
    - [>prior CMS experience]

### Professional Responsibility

- front-end engineering
    - HTML
        - HTML5
        - semantics
        - minimal tag count
    - CSS
        - organization & modularity
            - [Bem with Namespaces][BEMNS]
            - [ITCSS][]
            - [BEMIT][] <small>sans "Responsive Suffixes"</small>
            - OOCSS
            - <s>SMACSS</s>
            - <s>[>in-house]</s>
        - processing & compatibility
            1. [PostCSS][]
            2. SASS
            3. LESS
        - documentation & pattern library
            1. [KSS][]
            2. <s>[>styleguide.js]</s>
            3. <s>[>in-house]</s>
    - JavaScript
        - ECMAScript 5—6/2015+
            - ECMAScript 5
                <br /><small>The good parts and the bad parts.</small>
            - ECMAScript 2015
                <br /><small>I know, well, the \~half of it that I have used.</small>
            - ECMAScript 2016+
                <br /><small>Learning as neeeded.</small>
        - tooling
            1. Parcel
            1. Rollup
            1. Webpack
            1. <s>Bower</s>
            1. <s>Gulp</s>
            1. <s>Grunt</s>
            1. <s>RequireJS</s>
        - scripting
            1. [>DOM manipulation]
            1. NPM
            1. Node
        - latest technology
            - `.browserslistrc`
                - For JavaScript via Babel
                - For CSS via [PostCSS][]
            - ES2015+ via Babel
                - I've used its latest featureset for every new project.
                - I've had to integrate it into many tooling setups.
            - [`postcss-preset-env`][postcss-preset-env]
                - This, after having used [cssnext][].
                - I have begun to help the community.
            - Aurelia
                - This is, now, not a recent framework. But, I was an early adopter.
                - I required usage to be explicit, not its default implicit behavior.
        - APIs & Integration
            - internal APIs
            - Auth0
            - UX plugins
        - frameworks
            - See [JavaScript Frameworks][wes-js-fw].
    - usability
        - [>progressive enhancement]
        - [>accessibility]
        - performance
    - miscellaneous
        - linting
        - unit testing
- software architecture
    - decoupling & modularity
    - object-oriented & functional
    - composition vs. inheritence
    - maintenance vs. abstraction
    - cross-project DRY code
- version control
    1. GIT
        - GitFlow <small>and variations</small>
        - the rebase debate
        - everyday usage
        - advanced usage
        - submodules
    1. Mercurial, Subversion
        - No, thank you.
- information architecture
    - documentation
    - API design
    - pattern library
- CMS
    - [Craft CMS][craft], <small>extensive experience</small>
    - [Shopify][], <small>moderate experience</small>
    - WordPress, <small>off-and-on experience</small>
    - various static site generators, <small>researched before choosing Craft CMS</small>
    - <s>Joomla</s>
    - <s>PrestaShop</s>
    - <s>Magento</s>
    - <s>Typolight</s>
    - <s>and more</s>
- internationalization
    - linguistics
        - text direction
        - characters vs letters
        - grammatical differences
        - pluralization
    - translation
        - automated
        - manual
        - third-party service
        - API integration
- back-end engineering
    1. Node
    2. Python
    3. PHP
- scripting
    1. Bash
        <br /><small>Yes, then I learned Node.</small>
    2. Node
- cross-team communication
    - common vocabulary
    - professional sensitivity
        - others' professional goals
        - others' professional struggles
    - requirements gathering
        - documentation
        - conflict of interest
        - flexibility to change
    - planning & estimation
        - Agile & Scrum
        - resource management
- management
    - for large projects
    - for small teams
- ERP
    - <s>[>sinc.]<s>

### Professional Interest

- front-end engineering
    - web components
        - Polymer
        - ARIA
        - native support
    - latest technology
        - HTML
            - web components
            - polyfill `<details>`/`<summary>`
            - `<menu>` (for web apps)
        - CSS
            - `.paint()` API
            - Houdini API
            - polyfill CSS features
        - JavaScript
            - ES2019+
            - CSS Typed Object Model
            - server-side rendering
        - miscellaneous
            - HTTP2
            - [>hybrid mobile app frameworks]
            - progressive web apps
    - reduce dependencies
        - Implement semantic HTML first, then consider minimal JavaScript.
        - Drop jQuery for Zepto, until project code for DOM manip is raw JS.
        - Learn more Node so that projects use less Node packages.
    - miscellaneous
        - linting
        - unit testing
        - integration testing
- usability
    - accessibility
        - HTML5
        - [ARIA][]
        - non-screen `@media` features
    - cross-device
        - screen readers
        - mobile devices
        - TV, watches, etc.
    - and more
        - user experience
        - findability
        - content strategy
- API documentation
    - customer support
    - keep specs up-to-date
    - query language, like [GraphQL][]
- DevOps
    - CI/CD
    - cloud hosting
    - Linux containers
- software architecture
    - UML
    - web sequence diagrams
    - API design
- programming languages
    - TypeScript
    - C#
    - Scala
    - Ruby
- prototyping
    - sketch and digital wireframing
    - manual code-based functional prototyping
    - software-generated UX functional prototyping
- language & culture
    - localization
    - internationalization
    - translation
- design
    - atomic design
    - logos
    - icons
    - websites

### Amateur Experience

- design
    - [2012 Resume][wes-des-resume]
    - [2011 Website][wes-des-website]
    - [2010 Portfolio][wes-des-portfolio]
- language
    - linguistics
    - Chinese
    - Asian languages
- server-side web app frameworks
    1. KoaJS
    1. ExpressJS
    1. Flask
    1. CakePHP
    1. Ruby on Rails
- databases
    - MySQL
    - FileMaker
    - MongoDB

### Amateur Interest

- design
    - typography & fonts
    - iconography & logos
    - color theory & science

[craft]: https://craftcms.com "a very customizable CMS"
[Shopify]: https://www.shopify.com "a high quality e-commerce CMS"
[JSDoc]: https://jsdoc.app "JavaScript Doc Generator"
[phpdoc]: https://phpdoc.org "PHP Doc Generator"
[PostCSS]: https://postcss.org "Transform CSS with JS"
[cssnext]: https://cssnext.github.io "[Deprecated] Tomorrow’s CSS Today"
[KSS]: http://warpspire.com/kss "Kneath Style Sheets"
[BEM]: https://en.bem.info "Block Element Modifier"
[ITCSS]: http://www.creativebloq.com/web-design/manage-large-css-projects-itcss-101517528 "Inverted Triangle CSS"
[BEMIT]: https://csswizardry.com/2015/08/bemit-taking-the-bem-naming-convention-a-step-further "BEM & ITCSS & Namespaces"
[BEMNS]: https://csswizardry.com/2015/03/more-transparent-ui-code-with-namespaces "BEM with Namespaces"
[postcss-preset-env]: https://preset-env.cssdb.org "Tomorrow’s CSS Today"
[pep350]: https://www.python.org/dev/peps/pep-0350 "Codetags"
[ARIA]: https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA "Accessible Rich Internet Applications"
[GraphQL]: https://graphql.org "Query Language for APIs"
