
### <mark>Strengths</mark>

- organization
- simplify complex concepts
- calm when others are stressed
- documentation
- mentorship
- _never_ misspell "misspell" as "mispell"

### Weaknesses

- self-imposed pressure to succeed
- vernacular over jargon
- misspell `display` as `dispaly`
- misspell `master` as `amster`

### Physical

- Really, you're gonna read these?
    <b class="u-white-space--nowrap">Stop</b>.
- Glasses, all the better to
    <b class="u-white-space--nowrap">see you with</b>.
- Abled, but my hands may shake;
    <b class="u-white-space--nowrap">it's fun</b>.
