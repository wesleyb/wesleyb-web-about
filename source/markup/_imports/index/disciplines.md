<details class="c-skill" id="discipline--fe-dev">
    <summary class="c-skill__title">
        <h3 class="js-ignore"
            id="discipline--fe-dev--title"><mark>Front-End Engineering</mark></h3>
    </summary>
    <ul class="c-gauge--layout-stacked has-range--count-2 c-skill__data">
        <li class="c-gauge__range is-high">
            <label id="discipline--fe-dev--current-skill--label"
                for="discipline--fe-dev--current-skill"
                class="c-gauge__label"
                title="{{shared/title.current-proficiency.md}}">Current Proficiency</label>
            <progress id="discipline--fe-dev--current-skill"
                class="c-gauge__figure"
                aria-labelledby="discipline--fe-dev--title discipline--fe-dev--current-skill--label"
                value="4" max="5">
                <span class="c-gauge__desc">4 out of 5, relative to other disciplines</span>
            </progress>
        </li>
        <li class="c-gauge__range is-max">
            <label id="discipline--fe-dev--future-skill--label"
                for="discipline--fe-dev--future-skill"
                class="c-gauge__label"
                title="{{shared/title.future-proficiency.md}}">Projected Proficiency</label>
            <progress id="discipline--fe-dev--future-skill"
                class="c-gauge__figure"
                aria-labelledby="discipline--fe-dev--title discipline--fe-dev--future-skill--label"
                value="5" max="5">
                <span class="c-gauge__desc">5 out of 5, relative to other disciplines</span>
            </progress>
        </li>
    </ul>
    <aside class="c-skill__notes">
{{index/disciplines/fe-dev.md}}
    </aside>
</details>

<details class="c-skill" id="discipline--soft-arch">
    <summary class="c-skill__title">
        <h3 class="js-ignore"
            id="discipline--soft-arch--title">Software Architecture</h3>
    </summary>
    <ul class="c-gauge--layout-stacked has-range--count-2 c-skill__data">
        <li class="c-gauge__range">
            <label id="discipline--soft-arch--current-skill--label"
                for="discipline--soft-arch--current-skill"
                class="c-gauge__label"
                title="{{shared/title.current-proficiency.md}}">Current Proficiency</label>
            <progress id="discipline--soft-arch--current-skill"
                class="c-gauge__figure"
                aria-labelledby="discipline--soft-arch--title discipline--soft-arch--current-skill--label"
                value="3" max="5">
                <span class="c-gauge__desc">3 out of 5, relative to other disciplines</span>
            </progress>
        </li>
        <li class="c-gauge__range is-max">
            <label id="discipline--soft-arch--future-skill--label"
                for="discipline--soft-arch--future-skill"
                class="c-gauge__label"
                title="{{shared/title.future-proficiency.md}}">Projected Proficiency</label>
            <progress id="discipline--soft-arch--future-skill"
                class="c-gauge__figure"
                aria-labelledby="discipline--soft-arch--title discipline--soft-arch--future-skill--label"
                value="5" max="5">
                <span class="c-gauge__desc">5 out of 5, relative to other disciplines</span>
            </progress>
        </li>
    </ul>
    <aside class="c-skill__notes">
{{index/disciplines/soft-arch.md}}
    </aside>
</details>

<details class="c-skill" id="discipline--info-arch">
    <summary class="c-skill__title">
        <h3 class="js-ignore"
            id="discipline--info-arch--title">Information Architecture</h3>
    </summary>
    <ul class="c-gauge--layout-stacked has-range--count-2 c-skill__data">
        <li class="c-gauge__range is-low">
            <label id="discipline--info-arch--current-skill--label"
                for="discipline--info-arch--current-skill"
                class="c-gauge__label"
                title="{{shared/title.current-proficiency.md}}">Current Proficiency</label>
            <progress id="discipline--info-arch--current-skill"
                class="c-gauge__figure"
                aria-labelledby="discipline--info-arch--title discipline--info-arch--current-skill--label"
                value="2" max="5">
                <span class="c-gauge__desc">2 out of 5, relative to other disciplines</span>
            </progress>
        </li>
        <li class="c-gauge__range is-high">
            <label id="discipline--info-arch--future-skill--label"
                for="discipline--info-arch--future-skill"
                class="c-gauge__label"
                title="{{shared/title.future-proficiency.md}}">Projected Proficiency</label>
            <progress id="discipline--info-arch--future-skill"
                class="c-gauge__figure"
                aria-labelledby="discipline--info-arch--title discipline--info-arch--future-skill--label"
                value="4" max="5">
                <span class="c-gauge__desc">4 out of 5, relative to other disciplines</span>
            </progress>
        </li>
    </ul>
    <aside class="c-skill__notes">
{{index/disciplines/info-arch.md}}
    </aside>
</details>

<details class="c-skill" id="discipline--design">
    <summary class="c-skill__title">
        <h3 class="js-ignore"
            id="discipline--design--title">UX & Visual Design</h3>
    </summary>
    <ul class="c-gauge--layout-stacked has-range--count-2 c-skill__data">
        <li class="c-gauge__range">
            <label id="discipline--design--current-skill--label"
                for="discipline--design--current-skill"
                class="c-gauge__label"
                title="{{shared/title.current-proficiency.md}}">Current Proficiency</label>
            <progress id="discipline--design--current-skill"
                class="c-gauge__figure"
                aria-labelledby="discipline--design--title discipline-liux-des--current-skill--label"
                value="2" max="5">
                <span class="c-gauge__desc">3 out of 5, relative to other disciplines</span>
            </progress>
        </li>
        <li class="c-gauge__range is-high">
            <label id="discipline--design--future-skill--label"
                for="discipline--design--future-skill"
                class="c-gauge__label"
                title="{{shared/title.future-proficiency.md}}">Projected Proficiency</label>
            <progress id="discipline--design--future-skill"
                class="c-gauge__figure"
                aria-labelledby="discipline--design--titl  discipline-liux-des--future-skill--label"
                value="3" max="5">
                <span class="c-gauge__desc">4 out of 5, relative to other disciplines</span>
            </progress>
        </li>
    </ul>
    <aside class="c-skill__notes">
{{index/disciplines/design.md}}
    </aside>
</details>

<details class="c-skill" id="discipline--be-dev">
    <summary class="c-skill__title">
        <h3 class="js-ignore"
            id="discipline--be-dev--title">Back-End Engineering</h3>
    </summary>
    <ul class="c-gauge--layout-stacked has-range--count-2 c-skill__data">
        <li class="c-gauge__range is-low">
            <label id="discipline--be-dev--current-skill--label"
                for="discipline--be-dev--current-skill"
                class="c-gauge__label"
                title="{{shared/title.current-proficiency.md}}">Current Proficiency</label>
            <progress id="discipline--be-dev--current-skill"
                class="c-gauge__figure"
                aria-labelledby="discipline--be-dev--title discipline--be-dev--current-skill--label"
                value="2" max="5">
                <span class="c-gauge__desc">2 out of 5, relative to other disciplines</span>
            </progress>
        </li>
        <li class="c-gauge__range is-high">
            <label id="discipline--be-dev--future-skill--label"
                for="{{shared/title.future-proficiency.md}}"
                class="c-gauge__label"
                title="{{shared/title.future-proficiency.md}}">Projected Proficiency</label>
            <progress id="discipline--be-dev--future-skill"
                class="c-gauge__figure"
                aria-labelledby="discipline--be-dev--title discipline--be-dev--future-skill--label"
                value="4" max="5">
                <span class="c-gauge__desc">4 out of 5, relative to other disciplines</span>
            </progress>
        </li>
    </ul>
    <aside class="c-skill__notes">
{{index/disciplines/be-dev.md}}
    </aside>
</details>

<details class="c-skill" id="discipline--proj-mgmt">
    <summary class="c-skill__title">
        <h3 class="js-ignore"
            id="discipline--proj-mgmt--title">Project Management</h3>
    </summary>
    <ul class="c-gauge--layout-stacked has-range--count-2 c-skill__data">
        <li class="c-gauge__range">
            <label id="discipline--proj-mgmt--current-skill--label"
                for="discipline--proj-mgmt--current-skill"
                class="c-gauge__label"
                title="{{shared/title.current-proficiency.md}}">Current Proficiency</label>
            <progress id="discipline--proj-mgmt--current-skill"
                class="c-gauge__figure"
                aria-labelledby="discipline--proj-mgmt--title discipline--proj-mgmt--current-skill--label"
                value="2" max="5">
                <span class="c-gauge__desc">2 out of 5, relative to other disciplines</span>
            </progress>
        </li>
        <li class="c-gauge__range">
            <label id="discipline--proj-mgmt--future-skill--label"
                for="discipline--proj-mgmt--future-skill"
                class="c-gauge__label"
                title="{{shared/title.future-proficiency.md}}">Projected Proficiency</label>
            <progress id="discipline--proj-mgmt--future-skill"
                class="c-gauge__figure"
                aria-labelledby="discipline--proj-mgmt--title discipline--proj-mgmt--future-skill--label"
                value="3" max="5">
                <span class="c-gauge__desc">3 out of 5, relative to other disciplines</span>
            </progress>
        </li>
    </ul>
    <aside class="c-skill__notes">
{{index/disciplines/proj-mgmt.md}}
    </aside>
</details>

<details class="c-skill" id="discipline--tech-write">
    <summary class="c-skill__title">
        <h3 class="js-ignore"
            id="discipline--tech-write--title">Technical Writing</h3>
    </summary>
    <ul class="c-gauge--layout-stacked has-range--count-2 c-skill__data">
        <li class="c-gauge__range">
            <label id="discipline--tech-write--current-skill--label"
                for="discipline--tech-write--current-skill"
                class="c-gauge__label"
                title="{{shared/title.current-proficiency.md}}">Current Proficiency</label>
            <progress id="discipline--tech-write--current-skill"
                class="c-gauge__figure"
                aria-labelledby="discipline--tech-write--title discipline--tech-write--current-skill--label"
                value="3" max="5">
                <span class="c-gauge__desc">3 out of 5, relative to other disciplines</span>
            </progress>
        </li>
        <li class="c-gauge__range is-high">
            <label id="discipline--tech-write--future-skill--label"
                for="discipline--tech-write--future-skill"
                class="c-gauge__label"
                title="{{shared/title.future-proficiency.md}}">Projected Proficiency</label>
            <progress id="discipline--tech-write--future-skill"
                class="c-gauge__figure"
                aria-labelledby="discipline--tech-write--title discipline--tech-write--future-skill--label"
                value="4" max="5">
                <span class="c-gauge__desc">4 out of 5, relative to other disciplines</span>
            </progress>
        </li>
    </ul>
    <aside class="c-skill__notes">
{{index/disciplines/tech-write.md}}
    </aside>
</details>

<details class="c-skill" id="discipline--tech-prod">
    <summary class="c-skill__title">
        <h3 class="js-ignore"
            id="discipline--tech-prod--title">Technical Product Management</h3>
    </summary>
    <ul class="c-gauge--layout-stacked has-range--count-2 c-skill__data">
        <li class="c-gauge__range">
            <label id="discipline--tech-prod--current-skill--label"
                for="discipline--tech-prod--current-skill"
                class="c-gauge__label"
                title="{{shared/title.current-proficiency.md}}">Current Proficiency</label>
            <progress id="discipline--tech-prod--current-skill"
                class="c-gauge__figure"
                aria-labelledby="discipline--tech-prod--title discipline--tech-prod--current-skill--label"
                value="3" max="5">
                <span class="c-gauge__desc">3 out of 5, relative to other disciplines</span>
            </progress>
        </li>
        <li class="c-gauge__range">
            <label id="discipline--tech-prod--future-skill--label"
                for="discipline--tech-prod--future-skill"
                class="c-gauge__label"
                title="{{shared/title.future-proficiency.md}}">Projected Proficiency</label>
            <progress id="discipline--tech-prod--future-skill"
                class="c-gauge__figure"
                aria-labelledby="discipline--tech-prod--title discipline--tech-prod--future-skill--label"
                value="3" max="5">
                <span class="c-gauge__desc">3 out of 5, relative to other disciplines</span>
            </progress>
        </li>
    </ul>
    <aside class="c-skill__notes">
{{index/disciplines/tech-prod.md}}
    </aside>
</details>
