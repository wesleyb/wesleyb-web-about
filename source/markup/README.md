# WES Markup

## Notes

- The `_` prefix (common practice) describes files as included only at build-time.
- The `.markdown` extension prevents parsing of markdown by Parcel.
    - See [`parcel-bunddler` issue #2985](https://github.com/parcel-bundler/parcel/issues/2985).
