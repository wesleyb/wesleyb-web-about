# Wesley B — Icons

## Research Summary

To learn bare minimum requirements versus optional items, read [Google Developers … Browser Customization](https://developers.google.com/web/fundamentals/design-and-ux/browser-customization/).

- A single largest-size Apple Touch icon is adequate.
    - A simple icon
    - Put it in the root to avoid excess HTML; more than just Apple uses it.
- A single largest-size for Android Chrome is adequate.
    - But the size difference is kilobytes, so serve sizes via manifest.
- The tool [realfavicongenerator.net](https://realfavicongenerator.net) is overkill.
    - _"Not so fast. […] before dropping almost redundant 20 icons: how good is iOS at resizing pictures?"_ — [Favicon's blog](https://realfavicongenerator.net/blog/how-ios-scales-the-apple-touch-icon/)