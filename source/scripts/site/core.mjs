/**
 * Dynamically import HTML that was built from Markdown
 * @module site/core
 */
"use strict";

import MarkdownPage from '/scripts/_imports/book-page.mjs';

const page = new MarkdownPage();
