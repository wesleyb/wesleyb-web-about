/**
 * Create a page for a book document
 * @module _imports/book-page
 */
"use strict";

import * as markdownHTML from '/scripts/_imports/markdown-html.mjs';
import * as importHTML from '/scripts/_imports/import-html.mjs';

import HashBehavior from '/scripts/_imports/hash-behavior.mjs';
import ReferencesDialog from '/scripts/_imports/references-dialog.mjs';

/**
 * Import HTML template/page by name
 * @param {string} name
 */
function importTemplate(name) {
  const targetNode = document.getElementById(name);
  const importLink = document.querySelector('#import-' + name);
  const importedDoc = importLink && importLink.import;

  if (importedDoc) {
    console.debug(`Imported document "${name}":`, importedDoc);

    // importHTML.importNode(importedDoc, targetNode, 'template');
    importHTML.importNodes(importedDoc, targetNode);
  } else {
    console.warn(`Unable to find "${'#import-' + name}"`);
  }
}

/**
 * Manipulate HTML beyond Markdown native syntax support
 */
function manipulateHTML() {
  const footnotesElement = document.body.getElementsByClassName('footnotes')[0];

  markdownHTML.wrapNestedListsAsDetails();
  markdownHTML.wrapImplicitSectionsAsDetails();
  markdownHTML.activateDebug();
  markdownHTML.openLinksInNewWindow();
}

export default class BookPage {
  /** The name of the book document */
  _pageName = null;

  constructor(pageName = 'page-placeholder') {
    this._pageName = pageName;

    // Web Components
    // WARN: This module depends on a `<script>` tag loading the external module
    //       `@webcomponents/webcomponentsjs/webcomponents-lite.js` beforehand.
    // SEE: https://github.com/webcomponents/webcomponentsjs/issues/746
    if (window.WebComponents.ready) {
      this.bootstrap();
    } else {
      window.addEventListener('WebComponentsReady', this.bootstrap.bind(this));
    }
  }

  /**
   * Bootstrap page
   */
  bootstrap() {
    // The imports are loaded and the elements have been registered
    console.info('Web components are ready');

    this.init();
  }

  /**
   * Inititialize dynamic page behavior
   */
  init() {
    const hashBehavior = new HashBehavior();
    const referencesDialog = new ReferencesDialog();

    importTemplate(this._pageName);
    manipulateHTML();

    hashBehavior.init();
    referencesDialog.init();
  }
}
