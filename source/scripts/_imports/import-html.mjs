/**
 * Functions to use HTML imports
 * @module _imports/import-html
 */
"use strict";

/**
 * Tag names of elements that must not be imported
 * @type {string}
 */
const ILLEGAL_TAG_NAMES = ['html', 'head', 'body', 'title', 'base', 'meta'/*, 'style'*/];
// We want the <head>, because it may have <link>'s and <script>'s
// RFE: Consider `isLegalRootTagName()` to determine whether link is
//      "[body-ok](https://html.spec.whatwg.org/multipage/links.html#body-ok)"
/**
 * Tag names of elements that could have child elements allowed for import
 * @type {string[]}
 */
const LEGAL_ROOT_NAMES = ['head', 'body'];

/**
 * Classname(s) to show that JavaScript did stuff
 * @param {string}
 */
export const CLASSNAME = 'js-html-import';

/**
 * Log whether `HTMLImports` is supported
 */
function handleSupport() {
  const supportsImports = (function getImportFromLink() {
    return 'import' in document.createElement('link');
  });
  if (supportsImports) {
    console.info('Found support for importing HTML');
  } else {
    console.warn('Did not find support for importing HTML');
  }
}

/**
 * Remove all child nodes from node
 * @param {Node} node
 */
function emptyNode(node) {
  while (node.lastChild) {
    node.removeChild(node.lastChild);
  }
}

/**
 * Whether node is allowed to be imported
 * @param {Node} node
 */
function isAllowedImportNode(node) {
  const tagName = node.tagName && node.tagName.toLowerCase();
  const isElement = ( node.nodeType === Node.ELEMENT_NODE );
  const isAllowed = ( ILLEGAL_TAG_NAMES.indexOf(tagName) < 0 );

  return isElement && isAllowed;
}
/**
 * Whether node could have child nodes allowed for import
 * @param {Node} node
 */
function isAllowedRootNode(node) {
  const tagName = node.tagName && node.tagName.toLowerCase();
  const isElement = ( node.nodeType === Node.ELEMENT_NODE );
  const isAllowed = ( LEGAL_ROOT_NAMES.indexOf(tagName) >= 0 );

  return isElement && isAllowed;
}
/**
 * Whether node is the <html> root node
 * @param {Node} node
 */
function isHtmlRootNode(node) {
  const nodeName = node.nodeName && node.nodeName.toLowerCase();
  const isHtml = ( nodeName == 'html' );

  return isHtml;
}

/**
 * Mark the target node as having its HTML imported via JavaScript
 * @param {Node} node
 */
function tagTargetNode( node ) {
  node.classList.add(CLASSNAME);
}

/**
 * Setup any HTML import work:
 * 
 * - log messages about HTML import support
 */
function setup(targetNode) {
  handleSupport();
}

/**
 * Cleanup any HTML import work:
 * 
 * - tag specific document node as having been manipulated
 * - fix most browsers not registering internal links on imported HTML
 * @param {HTMLElement} targetNode
 */
function cleanup(targetNode) {
  tagTargetNode(targetNode);
}

/**
 * Append allowed nodes from imported doc to target document node
 *
 * _Many error checks: We do not trust the browser to import consistently._
 * @param {Document} importedDoc
 * @param {HTMLElement} targetNode
 */
export function importNodes(importedDoc, targetNode) {
  setup();

  let nodes = [];
  let rootNodes = [];
  let importedNodes = [...importedDoc.childNodes];
  // The first node allows us to identify how much of a document we have
  const isRootDocument = isHtmlRootNode(importedNodes[0]);

  // It would be bad practice to append a complete HTML root element
  if ( isRootDocument ) {
    console.info('Imported document contains root element (will attempt to reduce)');

    console.info('Reducing imported document to root node');
    importedNodes = [...importedDoc.documentElement.childNodes];

    console.info('Reducing root node to allowed child nodes');
    importedNodes.map( node => {
      const isAllowed = isAllowedRootNode(node);

      if ( isAllowed ) {
        rootNodes.push(...node.childNodes);
      }
    });
  } else {
    // Clone the array so we do not ruin the original (faster to do this without iteration)
    // FAQ: If you want to use spread syntax, be sure that the one-level deep copy is not an issue
    // SEE: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_syntax#Copy_an_array
    rootNodes = importedNodes.slice(0);
  }

  // We will blindly append all content nodes, so choose carefully
  nodes = rootNodes.filter( node => isAllowedImportNode(node) );

  // Before importing nodes, remove any existing outdated or "Loading…" UI
  emptyNode(targetNode);
  nodes.forEach( node => {
    const clone = document.importNode(node, true);
    targetNode.appendChild(clone);
  });

  cleanup(targetNode);
}

/**
 * Append node selected from imported doc to specific document node
 *
 * _Few error checks: We trust the user to select the correct node._
 * @param {Document} importedDoc
 * @param {HTMLElement} targetNode
 * @param {string} sourceNodeSelector
 */
export function importNode(importedDoc, targetNode, sourceNodeSelector) {
  setup();

  const sourceNode = importedDoc.querySelector(sourceNodeSelector);
  const clone = document.importNode(sourceNode, true);
  targetNode.appendChild(clone);

  cleanup(targetNode);
}
