/**
 * Object to create and update a dialog for viewing document references
 * @module _imports/references-dialog
 */
"use strict";

import cssEscape from 'css.escape';
import dialogPolyfill from 'dialog-polyfill';

/**
 * Classname(s) to show that JavaScript did stuff
 * @param {object}
 */
export const CLASSNAME = 'js-dialog';

/**
 * Create the dialog element
 * @param {HTMLElement} scopeElement - The element in which to create the dialog
 */
function createDialog(scopeElement) {
  let dialog = document.createElement('dialog');
      dialog.classList.add(CLASSNAME);
      dialog = scopeElement.appendChild(dialog);

  dialogPolyfill.registerDialog(dialog);

  return dialog;
}

export default class ReferencesDialog {
  /** The dialog for the book notes */
  _dialog = null;

  /** The HTML element to which the dialog belongs */
  _scopeElement = null;

  /* Whether the dialog is open */
  get isOpen() {
    return this._dialog.open;
  }

  /**
   * @param {HTMLElement} scopeElement = document.body
   */
  constructor(scopeElement = document.body) {
    this._dialog = createDialog(scopeElement);
    this._scopeElement = scopeElement;
  }

  /**
   * Define behavior for hover over footnote/glossary anchor hyperlink:
   *
   * - Hide/show footnotes/glossary dialog.
   * @param {HTMLElement} link - The hyperlink over which user hovered
   */
  handleLinkHover(link) {
    const hash = link && link.hash;

    this.trigger(hash);
  }

  /**
   * Inititialize hash behavior
   */
  init() {
    const links = this._scopeElement.querySelectorAll('a.glossary, a.footnote');

    [...links].forEach((link) => {
      link.addEventListener('mouseover', (event) => {
        this.handleLinkHover(link);
      });
      link.addEventListener('mouseout', (event) => {
        this.handleLinkHover();
      });
      link.addEventListener('touchstart', (event) => {
        this.handleLinkHover(link);
      });
      link.addEventListener('touchend', (event) => {
        this.handleLinkHover();
      });
    });
  }

  /**
   * Create the HTML content (as a node) for the dialog based on given ID
   * @param {string} id - ID of the element from which to build content
   * @return {HTMLElement}
   */
  buildContent(id) {
    const escapedId = cssEscape(id);
    const element = this._scopeElement.querySelector('#' + escapedId);
    const content = element.cloneNode(true);

console.warn('buildContent', {
  element: element,
  escapedId: escapedId,
  content: content
});

    return content;
  }

  /**
   * Update the dialog to have the given content
   * @param content {HTMLElement} - The content for the dialog
   */
  setContent(content) {
    this.clearContent();
    this.addContent(content);

    if ( ! this.isOpen) {
      this.open();
    }
  }

  /**
   * Add the given content to the dialog
   * @param content {HTMLElement} - The content to add
   */
  addContent(content) {

console.warn('addContent', {
  content: content,
  content_childNodes: content.childNodes
});
 
    while (content.childNodes.length > 0) {
      const node = content.childNodes[0];

console.warn('addContent:while', {
  node: node
});

      this._dialog.appendChild(node);
    }
  }

  clearContent() {

    console.warn('clearContent', {
      _dialog: this._dialog,
      _dialog_childNodes: this._dialog.childNodes
    });

    while (this._dialog.childNodes.length > 0) {
      const node = this._dialog.childNodes[0];

console.warn('clearContent:while', {
  node: node
});

      // NOTE: Do we need to `delete` return value to clean memory?
      //       Maybe the function scope avoids the need for this;
      //       The code `delete exampleReturnValueNode;` causes build
      //       error: "Deleting local variable in strict mode"
      this._dialog.removeChild(node);
    }

    if (this.isOpen) {
      this.close();
    }
  }

  /**
   * Close the dialog
   */
  close() {
    this._dialog.close();
  }

  /**
   * Open the dialog
   */
  open() {
    this._dialog.show();
  }

  /**
   * Either update and open, or clear and close, the dialog
   * @param {Location.hash} hash
   */
  trigger(hash) {
    const id = hash && hash.substring(1); // Removes `#`
    const element = id && this.buildContent(id);

console.warn('trigger', {
  id: id,
  element: element,
  this: this
});

    if (element) {
      this.setContent(element);
    } else {
      this.clearContent();
    }
  }
}
