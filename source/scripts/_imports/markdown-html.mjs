/**
 * HTML Manipulation for HTML that is generated from a Markdown document
 * 
 * _Markdown does not have a syntax for some HTML. This module can convert basic HTML into advanced HTML._
 * @module _imports/markdown-html
 */
"use strict";

/**
 * Classname(s) to show that JavaScript did stuff
 * @param {object}
 */
export const CLASSNAME = {
  details: {
    base: 'js-details-wrap',
    mod: {
      list: 'js-details-wrap--nested-list',
      heading: 'js-details-wrap--heading'
    },
    child: {
      summary: 'js-details-wrap__summary'
    }
  },
  debug: {
    showComments: 'js-show-comments'
  },
  ignore: 'js-ignore'
};

/**
 * Of given element, get the next element sibling that is matched by the selector
 * @param {HTMLElement} element
 * @param {string} selector
 * @see https://gomakethings.com/finding-the-next-and-previous-sibling-elements-that-match-a-selector-with-vanilla-js/
 */
function getNextSibling(element, selector) {
  var sibling = element.nextElementSibling;

  if ( ! selector) {
    return sibling;
  }

  while (sibling) {
    if ( sibling.matches(selector) ) {
      return sibling;
    }
    sibling = sibling.nextElementSibling;
  }
};

/**
 * Convert nested lists to `<details>` (and first child to `<summary>`)
 *
 * This function acts on `'li > ul`, `li > ol`, and `li > dl`.
 * @example
 * // BEFORE: A `<ul>` inside an `<li>`
 * <?l>
 *   <li>
 *     <p>First child will be put inside a "summary" tag.</p>
 *     <ul>
 *       <li>Alice</li>
 *       …
 *     </ul>
 *   </li>
 * </?l>
 * // AFTER: A `<ul>` inside `<details>` (with first child as `<summary>`) inside an `<li>`
 * <?l>
 *   <li>
 *     <details>
 *       <summary>
 *         <p>First child has been put inside a "summary" tag.</p>
 *       </summary>
 *       <ul>
 *         <li>Alice</li>
 *         …
 *       </ul>
 *     </details>
 *   </li>
 * </?l>
 */
export function wrapNestedListsAsDetails() {
  const nestedLists = document.querySelectorAll(`li > ul:not(.${CLASSNAME.ignore}), li > ol:not(.${CLASSNAME.ignore}), li > dl:not(.${CLASSNAME.ignore}):not(:only-child)`);
  const classNamesForDetails = [CLASSNAME.details.base, CLASSNAME.details.mod.list];
  const classNamesForSummary = [CLASSNAME.details.child.summary];

  nestedLists.forEach( nestedList => {
    const parentNode = nestedList.parentNode;
    const detailsElement = document.createElement('details');
          detailsElement.classList.add(...classNamesForDetails);
    const summaryElement = document.createElement('summary');
          summaryElement.classList.add(...classNamesForSummary);
    let elementsReviewCount = 0;

    /**
     * This will capture text nodes and element nodes, which causes a caveat.
     * 
     * > If first piece of text of list item has inline tags,
     * > then make sure that text is a paragraph not a text node.
     * @example
     * // Expectation
     * //// Markdown
     * - I have some text.
     *     - I am child text.
     * //// Initial Markup
     * <li>I have some text.
     *   <ul>
     *     <li>I am child text.</li>
     *   </ul>
     * </li>
     * //// Converted Markup
     * <li>
     *   <details>
     *     <summary>I have some text.</summary>
     *     <ul>
     *       <li>I am child text.</li>
     *     </ul>
     *   </details>
     * </li>
     * // Caveat
     * //// Markdown
     * - I have **bold** text.
     *     - I am child text.
     * //// Initial Markup
     * <li>I have <strong>bold<strong> text.
     *   <ul>
     *     <li>I am child text.</li>
     * </li>
     * //// Converted Markup
     * <li>
     *   <details>
     *     <summary>I have some</summary>
     *     <strong>bold<strong>
     *     text.
     *     <ul>
     *       <li>I am child text.</li>
     *     </ul>
     *   </details>
     * </li>
     */
    while (parentNode.firstChild) {
      const firstChild = parentNode.firstChild;
      const isSummary = (elementsReviewCount === 0);

      if (isSummary) {
        summaryElement.appendChild(firstChild);
        detailsElement.appendChild(summaryElement);
      } else {
        detailsElement.appendChild(firstChild);
      }

      elementsReviewCount++;
    }
    parentNode.appendChild(detailsElement);
  });
}

/**
 * Make content under heading be disclosable (using `<details>` and `<summary>`)
 * @example
 * // BEFORE: An `<h2>` followed one or more elements followed by another `<h2>`
 * <h2>Heading (except "h1") will be put inside a "summary" tag.</p>
 * <!-- Content will be put inside surrounding "details" tag. -->
 * <p>…</p>
 * <ul>…</ul>
 * <h2>Heading (except "h1") will be put inside a "summary" tag.</p>
 * <!-- Content will be put inside surrounding "details" tag. -->
 * …
 * <h1>…</h1>
 * // BEFORE: An `<h2>` inside a `<summary>` inside a `<details>` followed similar markup
 * <details>
 *   <summary>
 *     <h2>Heading (not "h1") has been put inside a "summary" tag.</h2>
 *   </summary>
 *   <!-- Content has been put inside surrounding "details" tag. -->
 *   <p>…</p>
 *   <ul>…</ul>
 * </details>
 * <details>…</details>
 * <h1>…</h1>
 */
export function wrapImplicitSectionsAsDetails() {
  const headingTags = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'];
  const nextHeadingSelector = 'h1, h2, h3, h4, h5, h6';
  const sectionHeadingSelector = `h3:not(.${CLASSNAME.ignore}), h4:not(.${CLASSNAME.ignore}), h5:not(.${CLASSNAME.ignore}), h6:not(.${CLASSNAME.ignore})`;
  const sectionHeadings = document.querySelectorAll(sectionHeadingSelector);
  const classNamesForDetails = [CLASSNAME.details.base, CLASSNAME.details.mod.heading];
  const classNamesForSummary = [CLASSNAME.details.child.summary];

  sectionHeadings.forEach( heading => {
    const parentNode = heading.parentNode;
    const nextHeading = getNextSibling(heading, nextHeadingSelector);
    let detailsElement = document.createElement('details');
        detailsElement.classList.add(...classNamesForDetails);
    let summaryElement = document.createElement('summary');
        summaryElement.classList.add(...classNamesForSummary);

    // Do not build a `<details>` if the
    // if () {
    //   return;
    // }

    while (heading.nextElementSibling) {
      const nextSibling = heading.nextElementSibling;
      const tagName = nextSibling.tagName && nextSibling.tagName.toLowerCase();
      const isSummary = ( headingTags.indexOf(tagName) >= 0);

      if (isSummary) {
        // ???: Why not insert before first child, right now?
        summaryElement = detailsElement.insertBefore(summaryElement, detailsElement.firstChild);
        break;
      } else {
        detailsElement.appendChild(nextSibling);
      }
    }
    summaryElement.appendChild(heading);
    // ???: Why not insert before first child, during loop?
    summaryElement = detailsElement.insertBefore(summaryElement, detailsElement.firstChild);
    detailsElement = parentNode.insertBefore(detailsElement, nextHeading);
  });
}

/**
 * Scroll to top of given `<details>` element when openned
 * @param {HTMLElement|HTMLElement[]} [details] - If not provided, will find all
 */
export function scrollToDetails(details) {
  let detailsElements;

  if ( ! details) {
    detailsElements = document.getElementsByTagName('details');
  } else if ( ! Array.isArray(details) ) {
    detailsElements = [details];
  } else {
    detailsElements = details;
  }

  [...detailsElements].forEach(element => {
    element.addEventListener('toggle', (event) => {
      if (element.open) {
        element.scrollIntoView({
          behavior: 'smooth'
        });
      }
    });
  });
}

/**
 * Open links in new windows by adding missing HTML
 * @param {HTMLElement} scopeElement = document.body
 */
export function openLinksInNewWindow(scopeElement = document.body) {
  const links = scopeElement.querySelectorAll(`
    a[href^="http"],
    a[href*="markup"],
    a[href$=".png"],
    a[href$=".jpg"]
  `);

  [...links].forEach((link) => {
    link.setAttribute('target', '_blank');
  });
}

/**
 * Activate debugging
 * 
 * _Add `js-` classes based on query parameters._
 */
export function activateDebug() {
  const searchParams = new URLSearchParams(window.location.search);
  const classNamesKnown = CLASSNAME.debug;

  for (name in classNamesKnown) {
    let shouldDebug = (searchParams.get(name) !== null);

    if (shouldDebug) {
      let classToAdd = classNamesKnown[name];

      document.body.classList.add(classToAdd);
    }
  }
}
