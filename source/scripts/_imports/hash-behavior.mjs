/**
 * Functions to react to URL hash change
 * @module _imports/hash-behavior
 */
"use strict";

import cssEscape from 'css.escape';

/**
 * Classname(s) to show that JavaScript did stuff
 * @param {object}
 */
export const CLASSNAME = {
  target: 'js-target'
};

/**
 * Get an ID from a `HTMLHyperlinkElementUtils.hash` that is escaped
 * @param {HTMLHyperlinkElementUtils.hash} hash
 * @return {string}
 */
function getIdFromHash(hash) {
  const id = hash && hash.substring(1); // Removes `#`

  return id;
}

/**
 * Return reference to the element targeted by the hash
 * @param {HTMLHyperlinkElementUtils.hash} hash
 * @param {object} [options]
 * @param {HTMLElement} [options.elementScope] - To where should search be scoped
 */
function getTarget(hash, {elementScope}) {
  const id = getIdFromHash(hash);
  const escapedId = cssEscape(id);
  const target = elementScope.querySelector('#' + escapedId);

  return target;
}

/**
 * Scroll to an element (optionally within a certain element), matched by given hash
 * @param {HTMLHyperlinkElementUtils.hash} hash
 * @param {object} [options]
 * @param {HTMLElement} [options.elementScope=document.body] - To where should search be scoped
 */
function scrollToAnchor(hash, {elementScope}) {
  const id = getIdFromHash(hash);
  const escapedId = cssEscape(id);
  const scrollTarget = elementScope.querySelector('#' + escapedId);

  if (scrollTarget) {
    console.info(`Scrolling to dynamically imported element whose id matches "${id}" i.e. \`${escapedId}\``, scrollTarget);

    scrollTarget.scrollIntoView({
      behavior: 'smooth'
    });
  } else {
    console.info(`Did not find element whose id matches "${id}" i.e. \`${escapedId}\`; will not scroll`);
  }
}

/**
 * Toggle which element is targeted (add/remove a class)
 *
 * _This is a way to duplicate the functionality of `:target` for dynamically-added HTML._
 * @param {HTMLHyperlinkElementUtils.hash} oldHash
 * @param {HTMLHyperlinkElementUtils.hash} newHash
 * @param {object} [options]
 * @param {HTMLElement} [options.elementScope] - To where should search be scoped
 */
function toggleTarget(oldHash, newHash, {elementScope}) {
  let target;

  if (oldHash) {
    target = getTarget(oldHash, {elementScope: elementScope});
    console.info('Toggling element:', target);
    target.classList.remove( CLASSNAME.target );
  }
  if (newHash) {
    target = getTarget(newHash, {elementScope: elementScope});
    console.info('Toggling element:', target);
    target.classList.add( CLASSNAME.target );
  }
}

/**
 * Set pathless anchor links to act on current page (bypass `<base>`)
 * 
 * Rewriting these anchor links to bypass `<base>` simplifies processing.
 * This is not an isolated use case; see https://stackoverflow.com/q/8108836 .
 * @param {HTMLHyperlinkElementUtils.hash} hash
 * @param {object} [options]
 * @param {HTMLElement} [options.elementScope=document.body] - To where should search be scoped
 */
function rewriteAnchorLinks({elementScope}) {
  const anchorLinks = elementScope.querySelectorAll('a[href*="#"]');

  [...anchorLinks].forEach((anchorLink) => {
    const relativePathname = window.location.href.split('#')[0];
    anchorLink.href = relativePathname + anchorLink.hash;
  });
}

export default class HashBehavior {
  /** The HTML element in which to confine the behavior */
  _scopeElement = null;

  /**
   * @param {HTMLElement} scopeElement = document.body
   */
  constructor(scopeElement = document.body) {
    this._scopeElement = scopeElement;
  }

  /**
   * Define behavior for change of URL hash:
   * 
   * - Replicate internal anchor link scroll functionality.
   *    - Firefox and Safari (tested 2019-05-11)
   *      do not support internal anchor links on any imported HTML.
   * - Hide/show footnotes/glossary dialog.
   * @param {HashChangeEvent} event
   */
  handleHashChange(event) {
    /* RFE: Use `newURL.hash` and `oldURL.hash` directly */
    const oldURL = (event) ? new URL( event.oldURL ) : undefined;
    const oldHash = (oldURL) ? oldURL.hash : null;
    /* RFE: Change this to use `event.newURL` */
    /* WARNING: Without unit tests, you'll need to manually test exisitng hash behavior */
    const newHash = window.location.hash;

    if (newHash) {
      scrollToAnchor(newHash, {elementScope: this._scopeElement});
    }

    toggleTarget(oldHash, newHash, {elementScope: this._scopeElement});
  }

  /**
   * Define behavior for links that would change URL hash:
   * 
   * - Set hash to act on current page (bypass `<base>`)
   */
  handleAnchorLinks() {
    rewriteAnchorLinks({elementScope: this._scopeElement});
  }

  /**
   * Inititialize hash behavior
   */
  init() {
    this.handleHashChange();
    this.handleAnchorLinks();

    window.addEventListener('hashchange', (event) => {
      this.handleHashChange(event);
    });
  }
}
