/**
 * HTML Accessibility for HTML that is not accessible enough.
 * 
 * _Some HTML features, like `<attr>` tag, are not accessible on some devices._
 * @module _imports/accessible-html
 */
"use strict";

// import * as abbrTouch from 'abbr-touch';

/**
 * Allow `<abbr>` accessibility for mobile devices.
 * 
 * _To limit scope (default scope is entire docuemnt), pass an ancestor element._
 * @param {HTMLElement} scopeElement
 * @example
 * <script src="node_modules/abbr-touch/abbr-touch.js"></script>
 * <script>
 * abbrTouch();
 * </script>
 * @see https://github.com/Tyriar/abbr-touch (may be incompatible with Parcel)
 */
export function enhanceAbbrTags(scopeElement) {

console.warn(abbrTouch);

  if ( scopeElement ) {
    // abbrTouch.abbrTouch( scopeElement );
  } else {
    // abbrTouch.abbrTouch();
  }
}
