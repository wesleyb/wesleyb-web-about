/**
 * WES CSS - cssnano Configuration
 * @see [cssnano](https://cssnano.co/)
 * @see [PostCSS: Configuration](https://cssnano.co/guides/presets)
 * @module config/cssNano
 */
"use strict";

const defaultPreset = require('cssnano-preset-default');

module.exports = defaultPreset({
    /* FAQ: This prevented broken CSS - 2019-07 */
    mergeLonghand: false,
    // RFE: Review outdated reference
    // SEE: https://github.com/MoOx/postcss-cssnext/issues/323
    autoprefixer: false,
    discardDuplicates: true,
    safe: true
});