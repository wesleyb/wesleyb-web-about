# WES - About

## Introduction

This is a resume website for Wesley B.

It uses [Node.js][nodejs], [ESLint][eslint] _(coming soon)_, and [some CSS linter][csslint] _(coming soon)_.

### Getting Started

- Read all of the "Important" section.
- Read and obey all of the "Developer Rules" section.
- Read the "Quick Start" section to begin using this project.

## Important

Final test of a functional change **must** be the command in **"Quick Start"**.

## Developer Rules

- Use [PEP 350](https://www.python.org/dev/peps/pep-0350/) to prefix comments.
- Follow all [Style Guides][yi3-style-guides].
- Integrate [ESLint][eslint] into code editor.
- Integrate [some CSS linter][csslint] into code editor (comming soon).

## Directory Structure

These directories are manually maintained.

    ./
       |_ public              // user-facing files
       |_ scripts             // JavaScript and configuration
       |_ styles              // stylesheets and configuration

## Requirements

- [Node][nodejs] 10.X+ _(do **not** exceed LTS)_
    - [NPM](https://docs.npmjs.com/getting-started/installing-node#updating-npm) 6.X+ _(or latest available for given Node version)_

## Quick Start

Build optimized user-facing static content using the command most appropriate for your host:

- Node: `npm install --unsafe-perm && npm run start`¹

**The result is static user-facing content at `./dist`.**

> ¹ The `--unsafe-perm` flag is only necessary for running the command as root/sudo.

## Development

### Notice

To be effectual, the following commands:

- **must** be run from this (`./`) directory
- **must** have been preceeded by the command `npm install`¹ _at least once_

> ¹ If `npm install` produces an error that mentions `cannot run in wd`, you are likely trying to run the command as root/sudo. If so, add the flag `--unsafe-perm`.

### Commands

#### Start via `npm start`

Run a local instance of the website.

#### Build via `npm run build`

Build static content and put it in `./dist`.

#### Install Dependencies via `npm install`

Install dependencies for all projects and sub-projects.

> If adding a new dependency, then use the flag `--save-dev` (compile time dependency) **or** `--save` (runtime dependency).

## Footnotes

1. For Windows, ensure that `node.exe` is on the `PATH` system variable.
2. Do **not** install dependencies globally.



[nodejs]: https://nodejs.org/ "Node.js"

[eslint]: http://eslint.org/ "ESLint"
[csslint]: https://www.google.com/search?q=css+linter "some CSS linter"

[yi3-style-guides]: http://welseyb.io/developer/style-guides "Yi3: Style Guides"
